import { Card, Col, Button, Nav, Form } from 'react-bootstrap';
import { NavLink, Link, useNavigate } from 'react-router-dom';
import { useContext, useState } from 'react'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductDetails({ name, description, price, imgSrc, productId }) {

	const { user } = useContext(UserContext)
	const navigate = useNavigate()
	const img_src = `/images/${name.split(" ").join("_").toLowerCase()}.jpg`

	const [quantity, setQuantity] = useState(1)

	const addToOrder = async () => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/users/addToOrder/${productId}`,{
			method: "PUT",
		    headers: {
		        'Content-Type': 'application/json',
		        Authorization: `Bearer ${localStorage.getItem('token')}`
		    },
		    body: JSON.stringify({
		        quantity: quantity
		    })
		})
		const data = response.json()

		Swal.fire({
    			title: "Product was added to order",
    			icon: "success",
        	});
		navigate('/products')
	}

	return (
		
		<Col className="d-flex h-50 w-50 justify-content-between m-auto">
			<Card className="m-3 h-75 w-50 flex-fill">
		      <Card.Img className="img-fluid card-img" variant="top" src={img_src} />
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>	
		        <Card.Text>
		        	{description}
		        </Card.Text>	   
		        <Card.Text>
		        	PHP {price}
		        </Card.Text>
		        <div className="d-flex justify-content-center">
		        	<Link className="btn btn-primary mx-1 h-75" to={`/products`}>Back</Link>
		        	
		        	{ (!user.isAdmin) &&	
			        	<Button className="btn btn-success mx-2 h-75" onClick={addToOrder}>Add to Order</Button>
				    }

				    <Form>
				    	<Form.Group className="mb-1 d-flex m-auto h-50">
				          <Form.Label className="p-2 h-75" htmlFor="quantity">Quantity</Form.Label>
				          <Form.Control className="p-2 m-auto" id="quantity" placeholder="1" type="number" value={quantity} onChange={e => setQuantity(e.target.value)} />
				        </Form.Group>
				    </Form>
				    			    
		        </div>
		      </Card.Body>
		    </Card>
	    </Col>
	   
		)
}
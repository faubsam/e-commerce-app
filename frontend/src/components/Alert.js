import { Link } from 'react-router-dom';

export default function Alert({ data }) {
	return (
		<div className="alert alert-warning w-75 m-auto mt-5" role="alert">
  			<p className="h2" >{data.header}</p>
  			<p>{data.message}</p><br></br>
  			<Link to={`${data.destination}`} className="alert-link">{data.label}</Link>
		</div>
		)
}
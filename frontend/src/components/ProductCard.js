import { Card, Col, Button, Nav } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import { useContext } from 'react'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

import './ProductCard.css'

export default function ProductCard({ product }) {
	const { user } = useContext(UserContext);
	const { _id, name, description, price, isActive } = product;
	const img_src = `/images/${name.split(" ").join("_").toLowerCase()}.jpg`

	const activateProduct = async () => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
			method: "PUT",
			headers: {
		        Authorization: `Bearer ${localStorage.getItem('token')}`
		    },
		})
		const data = await response.json().catch(err => err)

		return data
	}

	const archiveProduct = async () => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`,{
			method: "PUT",
			headers: {
		        Authorization: `Bearer ${localStorage.getItem('token')}`
		    },
		})
		const data = await response.json().catch(err => err)

		return data
	}

	const deleteProduct = async () => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`,{
			method: "DELETE",
			headers: {
		        Authorization: `Bearer ${localStorage.getItem('token')}`
		    },
		})
		const data = await response.json().catch(err => err)

		Swal.fire({
    			title: "Product was deleted successfully",
    			icon: "success",
        	});

		return data
	}


	return (
		<Col className="d-flex">
			<Card style={{ width: '16rem' }} className="m-3 flex-fill ">
		      <Card.Img className="img-fluid card-img-custom" variant="top" src={img_src} />
		      <Card.Body className="light-brown">
		        <Card.Title className="dark-brown">{name}</Card.Title>		   
		        <Card.Text>
		        	&#8369; {price}
		        </Card.Text>
		        <div className="d-flex justify-content-center pt-2">
		        	<Link className="btn btn-primary mx-1" to={`/products/${_id}`}>Details</Link>
		        	{	(user.isAdmin) &&
		        		<Link className="btn btn-secondary mx-2" as={ NavLink } to={`/editProduct/${_id}`}>Edit</Link>
		        	}
		        	{ (user.isAdmin) && (!isActive) &&	
			        	<Button className="btn btn-success mx-2" onClick={activateProduct}>Activate</Button>
				    }
				    {
				    	(user.isAdmin) && (isActive) &&
				    	<Button className="btn btn-warning mx-2" onClick={archiveProduct}>Archive</Button>
				    }
				    {
				    	(user.isAdmin) &&
				    	<Button className="btn btn-danger mx-2" onClick={deleteProduct}>Delete</Button>
				    }
		        </div>

		      </Card.Body>
		    </Card>
	    </Col>
		)
}
import { useContext } from 'react';
import { Container, Navbar, Nav } from "react-bootstrap";
import { NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

export default function NavBar() {
	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand to="/home">Coffee Shop</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            <Nav.Link as={ NavLink } to="/products">Products</Nav.Link>
	            { (user.id !== null) ?
							<Nav.Link as={ NavLink } to="/logout">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={ NavLink } to="/login">Login</Nav.Link>
								<Nav.Link as={ NavLink } to="/register">Register</Nav.Link>
							</>
				}	
				{ (user.isAdmin) ?
					<Nav.Link as={ NavLink } to="/admin">Admin</Nav.Link>
					:
					<>
					</>
				}
				{ (!user.isAdmin) ?
					<Nav.Link as={ NavLink } to="/checkout">Checkout</Nav.Link>
					:
					<>
					</>
				}	
				{ (!user.isAdmin) ?
					<Nav.Link as={ NavLink } to="/myOrders">My Orders</Nav.Link>
					:
					<>
					</>
				}
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>

		)
}
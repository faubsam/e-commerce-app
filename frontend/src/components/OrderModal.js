import { Modal, Button } from 'react-bootstrap'
import { useState, useEffect } from 'react'


export default function OrderModal({ products }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [orderProducts, setOrderProducts] = useState([])
  const [name, setName] = useState('')
  const [price, setPrice] = useState(0)
  const [subtotal, setSubtotal] = useState(0)
  const [newProd, setNewProd] = useState({})


  useEffect(() => {
  	const getAllOrderProducts = () => {
	  	setOrderProducts(products.map(prod => {
			fetch(`${process.env.REACT_APP_API_URL}/products/${prod.productId}`, {
	  		headers: {
	    				Authorization: `Bearer ${ localStorage.getItem('token') }`
	    			}
	  		}).then(response => {
	  			return response.json()
	  		})
	  		.then(product => {
	  			setName(product.name)
			  	setPrice(product.price)
			  	setSubtotal(product.price * prod.quantity)
			  	setNewProd(product)

			  	return newProd
			  }).then(newProd => {

			  })
	  			return (
				<div className="text-center" >
					<p key={prod.id}>
	    			{prod.productName}   ||   &#8369; {prod.productPrice}   ||   Quantity: {prod.quantity}   ||   Subtotal: &#8369; {(prod.productPrice * prod.quantity)}
	    			</p>
	    			<hr />
				</div>
				)
		}))
	  }
	getAllOrderProducts()
	
  }, [show])
  

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Details
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {
			orderProducts
        }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
import './App.css';

import NavBar from './components/NavBar'
import Register from './pages/Register'
import Login from './pages/Login'
import Products from './pages/Products'
import Admin from './pages/Admin'
import Error from './pages/Error'
import Logout from './pages/Logout'
import AddProduct from './pages/AddProduct'
import EditProduct from './pages/EditProduct'
import ProductDetail from './pages/ProductDetail'
import Checkout from './pages/Checkout'
import MyOrders from './pages/MyOrders'
import AllOrders from './pages/AllOrders'
import SetAdmin from './pages/SetAdmin'

import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext'

function App() {
  const [ user, setUser ] = useState({
      id: null,
      isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    });
  }, [])

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <NavBar />
          <Routes>
            <Route path="/" element={<Products />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductDetail />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/allOrders" element={<AllOrders />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/checkout" element={<Checkout />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="/myOrders" element={<MyOrders />} />
            <Route path="/setAdmin" element={<SetAdmin />} />
            <Route path="/editProduct/:productId" element={<EditProduct />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

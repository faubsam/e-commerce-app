import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function EditProduct() {
	const { user } = useContext(UserContext);
	const navigate = useNavigate()
	const params = useParams()

	useEffect(() => {
		if (!user.isAdmin) {
			navigate("/error")
		}
	}, [])

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [isActive, setIsActive] = useState('')

	const editProduct = async (event) => {
		event.preventDefault()

		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${params.productId}`, {
			method: "PUT",
		    headers: {
		        'Content-Type': 'application/json',
		        Authorization: `Bearer ${localStorage.getItem('token')}`
		    },
		    body: JSON.stringify({
		        name: name,
		        description: description,
		        price: price
		    })

		})
		const data = await response.json()
		Swal.fire({
    			title: "Product was edited successfully",
    			icon: "success",
        	});
		navigate("/admin")
	}

	const getProductDetails = async (id) => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		const product = await response.json()
		setName(product.name)
		setDescription(product.description)
		setPrice(product.price)
	}

	useEffect(() => {
		getProductDetails(params.productId)
	}, [])

	useEffect(() => {
		
		if(name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price]);

	return (
		<Container fluid>
			<Form className="mb-3 w-75 mx-auto mt-3 rounded-2 border border-light p-2" onSubmit={(e) => editProduct(e)}>
			  <Form.Group className="mb-3 mt-3" controlId="name">
		        <Form.Label>Name</Form.Label>
		        <Form.Control type="text" placeholder="Product Name" value = {name} onChange={e => setName(e.target.value)} required/>
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="description">
		        <Form.Label>Description</Form.Label>
		        <Form.Control type="text" placeholder="Description" value = {description} onChange={e => setDescription(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control type="number" placeholder="PHP 0.00" value = {price} onChange={e => setPrice(e.target.value)} required/>
		      </Form.Group>

		    { isActive ?
	  			<Button variant="success" type="submit" id="submitBtn">
	  			  Save Changes
	  			</Button>
	  			:
	  			<Button variant="danger" type="submit" id="submitBtn" disabled>
	  			  Save Changes
	  			</Button>
	  		}
	    	</Form>
    	</Container>
		)
}
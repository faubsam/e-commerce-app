import { useState, useEffect, useContext } from 'react'
import { Table } from 'react-bootstrap'
import { NavLink, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import OrderModal from '../components/OrderModal'

export default function AllOrders() {
	const { user } = useContext(UserContext)
	const [orders, setOrders]  = useState([])

	useEffect(() => {
		const getAllOrders = async () => {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
				headers: {
    				Authorization: `Bearer ${ localStorage.getItem('token') }`
    			}
			})
			const data = await response.json()

			setOrders(data.map(order => {
				return (
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.userId}</td>
						{/*<td>{order.products}</td>*/}
						<td>&#8369; {order.totalAmount}</td>
						<td>{order.purchasedOn}</td>
						<td>
							<OrderModal products={ order.products }/>
						</td>
					</tr>
				)
			}))
		}
		getAllOrders()
	}, [])

	return (
		<div className="m-auto my-3 w-75">
		<h2 className="text-center p-2 mb-3">All Orders</h2>
	    <Table striped bordered hover>
	      <thead>
	        <tr>
	          <th>Order ID</th>
	          <th>User</th>
			  {/*<th>Details</th>*/}
	          <th>Total</th>
	          <th>Date</th>
	        </tr>
	      </thead>
	      <tbody>
	      	{ orders }
	      </tbody>
	    </Table>

	    <Link className="btn btn-primary w-25" as={ NavLink } to="/admin">Back</Link>
    </div>
		)
}
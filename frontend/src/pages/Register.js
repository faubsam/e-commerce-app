import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register(event) {
	const { user } = useContext(UserContext);
	const navigate = useNavigate()

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState('')

	const registerUser = async (event) => {
		event.preventDefault()

		const response = await fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    password: password
                })
		})

		const data = await response.json()
		
		if (data !== false) {
			setFirstName('');
            setLastName('');
            setEmail('');
            setPassword('');
            setPassword2('');

            Swal.fire({
                title: 'Registration successful',
                icon: 'success',
                text: 'Welcome to the coffee shop! You can now login using your email and password'
            });
            navigate("/login")
		} else {
			Swal.fire({
                title: 'Something wrong',
                icon: 'error',
                text: 'Please try again.'   
            });
		}
	}


	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password !== '' && password2 !== '' && firstName !== '' && lastName !== '') && (password === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password, password2, firstName, lastName]);

	return (
		(user.id !== null) ?
			<Navigate to="/login" />
		:
		<Container fluid>
			<Form className="mb-3 w-75 mx-auto mt-3 rounded-2 border border-light p-2" onSubmit={(e) => registerUser(e)}>
			  <Form.Group className="mb-3 mt-3" controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control type="text" placeholder="First Name" value = {firstName} onChange={e => setFirstName(e.target.value)} required/>
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control type="text" placeholder="Last Name" value = {lastName} onChange={e => setLastName(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value = {email} onChange={e => setEmail(e.target.value)} required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value = {password} onChange={e => setPassword(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control type="password" placeholder="Confirm Password" value = {password2} onChange={e => setPassword2(e.target.value)} required/>
		      </Form.Group>

		    { isActive ?
	  			<Button variant="primary" type="submit" id="submitBtn">
	  			  Register
	  			</Button>
	  			:
	  			<Button variant="danger" type="submit" id="submitBtn" disabled>
	  			  Register
	  			</Button>
	  		}
	    	</Form>
    	</Container>
		)
}
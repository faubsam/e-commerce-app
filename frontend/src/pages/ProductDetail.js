import { useEffect, useState, useContext } from 'react';
import { Navigate, useNavigate, useParams} from 'react-router-dom'
import { Row } from 'react-bootstrap'
import ProductDetails from '../components/ProductDetails'
import UserContext from '../UserContext';

export default function ProductDetail() {
	const params = useParams()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [imgSrc, setImgSrc] = useState('')
	const [product, setProduct] = useState('')

	const getProductDetails = async (id) => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		const product = await response.json()

		setName(product.name)
		setDescription(product.description)
		setPrice(product.price)
		setImgSrc(`/images/${name.split(" ").join("_").toLowerCase()}.jpg`)
	}

	useEffect(() => {
		getProductDetails(params.productId).catch(err => console.log(err))
	}, [])

	return (
		<ProductDetails name={name} description={description} price={price} imgSrc={imgSrc} productId={params.productId}/>
		)
}
import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {
	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate()

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    const authenticate = async (event, token) => {
    	event.preventDefault()

    	const response = await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    		method: 'POST',
        	headers: {
        		'Content-Type': 'application/json',
        	},
        	body: JSON.stringify({
        		email: email,
        		password: password
        	})
    	})

    	const data = await response.json()

    	if (typeof data.access !== "undefined") {
    		localStorage.setItem('token', data.access)
    		const userDetails = await retrieveUserDetails(data.access)
    		Swal.fire({
    			title: "Login Successful",
    			icon: "success",
    			text: "Welcome to the Coffee Shop!"
        	});
        	if (user.isAdmin) {
        		navigate('/admin')
        	} else {
        		navigate("/products")
        	}
    	} else {
    		Swal.fire({
    			title: "Authentication Failed",
    			icon: "error",
    			text: "Failed to verify your credentials"
    		});
    	}
    }

    const retrieveUserDetails = (token) => {

    	fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
    		headers: {
    			Authorization: `Bearer ${ token }`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {
    		setUser({
    			id: data._id,
    			isAdmin: data.isAdmin
    		})
    	})
    };


    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password]);

    return (

    	(user.id !== null) ?
    		<Navigate to="/products"/>
    	:
	    <Form className="mb-3 w-75 mx-auto mt-3 rounded-2 border border-light p-2" onSubmit={e => authenticate(e)}>
	        <Form.Group className="mb-3" controlId="email">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email"
	                value={email}
	    			onChange={(e) => setEmail(e.target.value)}
	                required
	            />
	        </Form.Group>

	        <Form.Group className="mb-3" controlId="password">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value={password}
	    			onChange={(e) => setPassword(e.target.value)}
	                required
	            />
	        </Form.Group>

	        { isActive ? 
	            <Button variant="success" type="submit" id="submitBtn">
	                Login
	            </Button>
	            : 
	            <Button variant="danger" type="submit" id="submitBtn" disabled>
	                Login
	            </Button>
	        }
	    </Form>
	)
}
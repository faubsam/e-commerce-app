import { Table, Button } from 'react-bootstrap';
import { useContext, useEffect, useState } from 'react'
import {useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Checkout() {
	const { user } = useContext(UserContext);
	const [userDetails, setUserDetails] = useState({})
	const [name, setName] = useState('')
	const [price, setPrice] = useState(0)

	const token = localStorage.getItem('token')
	const navigate = useNavigate()

	const getUserDetails = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
    		headers: {
    			Authorization: `Bearer ${ token }`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {
    		setUserDetails(data)

    	}).catch(err => err)
	}

	const getProductDetails = async (id) => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		const product = await response.json()
		setName(product.name)
		setPrice(product.price)
	}

	const checkoutOrder = async () => {
		const response = await fetch(`${process.env.REACT_APP_API_URL}/users/checkout`,{
			method: "POST",
			headers: {
    			Authorization: `Bearer ${ token }`
    		}
		})
		const checkout = await response.json()

		Swal.fire({
    			title: "Thanks for submitting your order!",
    			icon: "success"
        	});

		navigate('/products')
	}

	useEffect(() => {	
		getUserDetails()
		
	}, [])

	let total = 0
	return (
	<div className="m-auto my-5 w-75">
		{
	      		!userDetails.cart && 
	      		<h2>There are no orders yet.</h2>
	      	}
	    <Table striped bordered hover>
	      <thead>
	        <tr>
	          <th>Product</th>
	          <th>Quantity</th>
	          <th>Price</th>
	          <th>Subtotal</th>
	        </tr>
	      </thead>
	      <tbody>
	      	{
	      		userDetails.cart &&
	      		userDetails.cart.map(item => {
	      			total += (item.quantity * item.productPrice)
	      			return (
		      				<tr key={item.productId}>
					          <td>{item.productName}</td>
					          <td>{item.quantity}</td>
					          <td>&#8369; {item.productPrice}</td>
					          <td>{(item.quantity * item.productPrice)}</td>

					        </tr>
		        )
	      		})
	      	}
	      	
	      </tbody>
	    </Table>
	    <h3><strong>Total: &#8369; {total}</strong></h3>
	    <Button className="btn btn-primary w-25" onClick={checkoutOrder}>Confirm Order</Button>
    </div>
  );
}
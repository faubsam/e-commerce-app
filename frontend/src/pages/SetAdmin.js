import { useState, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { useNavigate, Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function SetAdmin() {
	const { users } = useContext(UserContext)
	const navigate = useNavigate()

	const [userId, setUserId] = useState('')

	const setAdmin = async (event) => {
		event.preventDefault()

		const response = await fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: userId
			})
		})

		const data = await response.json()

		Swal.fire({
    			title: "Changes have been saved" ,
    			icon: "success",
    			text: `User with ID ${userId} is now an admin`
        	});
		navigate("/admin")
	}


	return (
		<Form className="mb-3 w-75 mx-auto mt-3 rounded-2 border border-light p-2" onSubmit={(e) => setAdmin(e)}>
			  <Form.Group className="mb-3 mt-3 w-75" controlId="userId">
		        <Form.Label className="mb-2 fs-5">User ID</Form.Label>
		        <Form.Control type="text" placeholder="User ID" value = {userId} onChange={e => setUserId(e.target.value)} required/>
		      </Form.Group>

		      	<Button variant="success" type="submit" id="submitBtn">
	  			  Save Changes
	  			</Button>
		</Form>
		)
}
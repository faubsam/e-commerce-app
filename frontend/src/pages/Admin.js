import { useContext, useState, useEffect } from 'react';
import { Navigate, useNavigate, NavLink } from 'react-router-dom';
import { Row, Button, Nav } from 'react-bootstrap'
import UserContext from '../UserContext';
import ProductCard from '../components/ProductCard'

export default function Admin() {
	const { user } = useContext(UserContext);
	const navigate = useNavigate()

	useEffect(() => {
		if (!user.isAdmin) {
			navigate("/error")
		}
	}, [])

	const [products, setProducts] = useState([])

	useEffect(() => {
		const getAllProducts = async () => {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/products/getAllProducts`)
			const data = await response.json()

			setProducts(data.map((product) => {
				return (
					<ProductCard key={product._id} product={product}/>
				)
			}))
		}

		getAllProducts().catch(err => console.log(err))
	}, [products])
	

	
	return (
		<div className="p-2">
			<h1 className="text-center p-3 m-2">Welcome to the Admin page!</h1>
			<div className="d-flex justify-content-center">
				<Button className="btn btn-primary mx-3 my-4 w-25">
					<Nav.Link as={ NavLink } to="/addProduct">Create New Product</Nav.Link>
				</Button>
				<Button className="btn btn-secondary mx-3 my-4 w-25">
					<Nav.Link as={ NavLink } to="/allOrders">View All Orders</Nav.Link>
				</Button>
				<Button className="btn btn-warning mx-3 my-4 w-25">
					<Nav.Link as={ NavLink } to="/setAdmin">Set User As Admin</Nav.Link>
				</Button>
			</div>
			
			<Row lg={3} md={2}>
				{ products }
			</Row>
		</div>
		)

}